<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
</head>
<body>
	<div class="container">
		<h1 class="text-center mt-4">Curd Application using Ajax</h1>
		<div class="d-flex justify-content-end">
			<button class="btn btn-outline-primary" id="modal1_open">Add User</button>
		</div>

		<div class="mt-3" id="msg">
			
		</div>
		<div id="showdata" class="mt-2">
		</div>



		<!-- insert modal  -->
		<div class="modal fade" id="modal1">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h3 class="modal-title">New User</h3>
		        <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <label>First Name :-</label>
		        <input type="text" id="fname" class="form-control" placeholder="First Name">
		        <label>Last Name :-</label>
		        <input type="text" id="lname" class="form-control" placeholder="Last Name">
		        <label>Email :-</label>
		        <input type="text" id="email" class="form-control" placeholder="Email">
		        <label>Mobile No. :-</label>
		        <input type="text" id="mobile" class="form-control" placeholder="Mobile No.">
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-primary" id="insert" data-dismiss="modal">Register</button>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      </div>
		    </div>
		  </div>
		</div>

<!-- edit modal -->
		<div class="modal fade" id="edit_modal">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h3 class="modal-title">Update User</h3>
		        <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <label>First Name :-</label>
		        <input type="text" id="edit_fname" class="form-control" placeholder="First Name">
		        <label>Last Name :-</label>
		        <input type="text" id="edit_lname" class="form-control" placeholder="Last Name">
		        <label>Email :-</label>
		        <input type="text" id="edit_email" class="form-control" placeholder="Email">
		        <label>Mobile No. :-</label>
		        <input type="text" id="edit_mobile" class="form-control" placeholder="Mobile No.">
		        <input type="hidden" id="edit_id">
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-primary" id="edited_submit" data-dismiss="modal">Register</button>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      </div>
		    </div>
		  </div>
		</div>

	</div>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript">


			$('#modal1_open').click(function(){
				$('#modal1').modal('show');

			});

			$('#insert').click(function(){
				var fname = $('#fname').val();
				var lname = $('#lname').val();
				var mobile = $('#mobile').val();
				var email = $('#email').val();
				$.ajax({
					url:'backend.php?id=4',
					type:'post',
					data:{ fname :fname,lname:lname,mobile:mobile,email:email },
					success:function(data)
					{
						var data ='<div class="alert alert-success">The User Created Successfully....</div>';
						$('#msg').html(data).fadeOut(4000);
						$('#fname').val('');
						$('#lname').val('');
						$('#mobile').val('');
						$('#email').val('');
						showData();
					},
					error:function()
					{

					}
			});
				});


			showData();
			function showData()
			{
				$.ajax({
					url:'backend.php?id=1',
					success:function(data)
					{
						$('#showdata').html(data);
					},
					error:function()
					{
						alert('error');
					}
				});
			}

			function edited(id)
			{
				$.ajax({
					url:'backend.php?id=2',
					type:'post',
					data:{ ida: id},
					success:function(data)
					{
						var data1 = JSON.parse(data);
						$('#edit_fname').val(data1.fname);
						$('#edit_lname').val(data1.lname);
						$('#edit_mobile').val(data1.mobile);
						$('#edit_email').val(data1.email);
						$('#edit_id').val(data1.id);
						$('#edit_modal').modal('show');
					},
					error:function()
					{
						alert('error');
					}
				});
			}

			function deleted(id)
			{
				var deleted = confirm('Are you sure...');
				if (deleted == true) {
					$.ajax({
						url:'backend.php?id=5',
						type:'post',
						data:{ida:id},
						success:function(data){
							showData();
						}
					});
				}
			}

			$('#edited_submit').click(function(){
				var fname = $('#edit_fname').val();
				var lname = $('#edit_lname').val();
				var mobile = $('#edit_mobile').val();
				var email = $('#edit_email').val();
				var id = $('#edit_id').val();
				$.ajax({
					url:'backend.php?id=3',
					type:'post',
					data:{ fname :fname,lname:lname,mobile:mobile,email:email,ida:id },
					success:function(data)
					{
						var data ='<div class="alert alert-success">The User Updated Successfully....</div>';
						$('#msg').html(data).fadeOut(4000);
						showData();
					},
					error:function()
					{

					}
				});
			});

	</script>
</body>
</html>