<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
</head>
<body>
	<div class="container">
		<div class="offset-md-3 col-md-6 mt-5">
			<div class="form-group">
				<form>
					<label>
						Enter Your Username :-
					</label>
					<input type="text" name="" id="username" class="form-control">
					<br>
					<label>
						Enter Your Password :-
					</label>
					<input class="form-control" type="password" name="" id="password">
					<br>
					<input type="button" name="" id="show_passwprd" class="btn btn-primary" value="Show Password">
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#show_passwprd').click(function(){
				var pass_field = $('#password');
				
				if (pass_field.val() != '') {
					if (pass_field.attr('type') == 'password') {
						$('#show_passwprd').val('Hide Password');
						pass_field.attr('type','text');
					}
					else {
						$('#show_passwprd').val('Show Password');
						pass_field.attr('type','password');
					}
				}
				else
				{
alert('Please enter the password.');
				}
				
			})
		});
	</script>
</body>
</html>