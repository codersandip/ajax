<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="http://localhost/bootswatch-master/docs/3/cerulean/bootstrap.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
</head>
<body>
	<div class="container mt-5">
		<h2 class="text-center mb-3">Poll System using PHP And AJAX</h2>
		<div class="row mt-5">
			<div class="col-md-6">
				<h3 class="text-center">Which is The Best Framework 2019?</h3>
				<form method="post" id="poll_form">
					<div class="radio">
						<label><h4><input type="radio" name="poll_option" class="poll_option" value="Laravel">Laravel</h4></label>
					</div>
					<div class="radio">
						<label><h4><input type="radio" name="poll_option" class="poll_option" value="CakePHP">CakePHP</h4></label>
					</div>
					<div class="radio">
						<label><h4><input type="radio" name="poll_option" class="poll_option" value="CodeIgniter">CodeIgniter</h4></label>
					</div>
					<div class="radio">
						<label><h4><input type="radio" name="poll_option" class="poll_option" value="Smphony">Smphony</h4></label>
					</div>
					<div class="radio">
						<label><h4><input type="radio" name="poll_option" class="poll_option" value="Zend Framework">Zend Framework</h4></label>
					</div>
				</form>
			</div>
			<div class="col-md-6">
					<br>
					<br>
					<div id="poll_data">
						
					</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			fetch_poll_data();
			function fetch_poll_data()
			{
				$.ajax({
					url : "poll_fetch.php",
					success:function(data)
					{
						$('#poll_data').html(data);
					}
				});
			}
		});
	</script>
</body>

</html>