<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
</head>
<body>
	<div class="container">
		<input type="" id="text" name="" class="form-control m-3">
		<div id="data"></div>
	</div>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript">
		$(function(){
			$.ajax({
					url : "search_fetch.php",
					success:function(data)
					{
						$('#data').html(data);
					}
				});

			$('#text').keyup(function(){
				var text = $(this).val();
				$.ajax({
					url : "search_fetch.php",
					type:"post",
					data : {text:text},
					success:function(data)
					{
						$('#data').html(data);
					}
				});
			});
		});
	</script>
</body>
</html>